package com.example.group11lecture4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        init()

    }
    private fun init(){
        Glide.with(this).load("https://images.unsplash.com/photo-1573196444192-cc9f26e94408?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max").into(ImageView1)
        Glide.with(this).load("https://images.unsplash.com/photo-1572385008709-fc445242f7b3?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max").into(ImageView2)
        Glide.with(this).load("https://images.unsplash.com/photo-1574274575744-7ee525dc51d7?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max").into(ImageView3)
        Glide.with(this).load("https://images.unsplash.com/photo-1573764852047-592d46dabdf6?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max").into(ImageView4)
        Glide.with(this).load("https://digest.pia.ge/uploaded/daijesti/2018-01/201801031535262119486500.jpg").into(profileImageView)
        Glide.with(this).load("https://i.ytimg.com/vi/-_XL25a7qxM/maxresdefault.jpg").into(coverImageView)


    }
}
